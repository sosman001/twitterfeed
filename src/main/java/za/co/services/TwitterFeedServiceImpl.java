package za.co.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import za.co.domain.Tweet;
import za.co.domain.User;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Created by shabnam on 7/18/17.
 */
@Service
public class TwitterFeedServiceImpl
        implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TwitterFeedServiceImpl.class);
    Set<User> allUsers;

    @Autowired
    ResourceLoader resourceLoader;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        try {

            allUsers = new HashSet<User>();

            String path = System.getProperty("user.dir");

            Resource resource = resourceLoader.getResource("file://"+path+"/input/user.txt");

            loadUsers(resource);

            resource = resourceLoader.getResource("file://" + path + "/input/tweet.txt");

            loadTweets(resource);

            List<User> sortedUserList = sortUsersByUsername();

            displayUserFeeds(sortedUserList);

        } catch (Exception e) {
            LOGGER.error("File not found.");
            System.exit(0);
        }

    }

    private void displayUserFeeds(List<User> sortedUserList) {
        for (User user:sortedUserList) {

            System.out.println(user.getUsername());

            for(Tweet tweet:user.getFeed()){
                System.out.println("@"+tweet.getText());
            }
        }
    }

    private List<User> sortUsersByUsername() {

        List<User> sortedUserList = new ArrayList<User>(allUsers);
        Collections.sort(sortedUserList);

        return sortedUserList;
    }

    public void loadTweets(Resource resource) {
        BufferedReader br = null;

        try {
            br = new BufferedReader((new InputStreamReader(new FileInputStream(resource.getFile()))));

            br.lines().map(mapTweetToUserFeeds).collect(Collectors.toList());
        }
        catch (IOException e) {
            System.out.println("Could not load tweet file.");
            e.printStackTrace();
        }
        finally {
            try {
                br.close();
            } catch (IOException e) {
                System.out.println("Could not close buffered reader for tweet file #fail");
                e.printStackTrace();
            }
        }

    }

    public void loadUsers(Resource resource){

        List<User> userList;
        BufferedReader br = null;

        try {

            br = new BufferedReader(new InputStreamReader(new FileInputStream(resource.getFile())));

            userList = br.lines().map(mapToUser).collect(Collectors.toList());

            List<User> duplicates = userList.stream().filter(n -> !allUsers.add(n)).collect(Collectors.toList());

            for (User dup:duplicates) {

                allUsers.stream().filter(dup::equals).map(n -> n.getFollowers().add(dup.getUsername())).collect(Collectors.toList());
            }

            addUsersThatDontFollowOthers(allUsers);
        }
        catch (IOException e) {
            System.out.println("Could not load user file.");
            e.printStackTrace();
        }
        finally {
            try {
                br.close();
            } catch (IOException e) {
                System.out.println("Could not close buffered reader for user file #fail");
                e.printStackTrace();
            }
        }

    }

    private void addUsersThatDontFollowOthers(Set<User> allUsers) {
        for (User user:allUsers) {
            for (String username:user.getFollowers()) {
                User u = new User(username);
                allUsers.add(u);
            }
        }
    }

    private Function<String, User> mapToUser = (line) -> {

        String [] parts = line.split(" ",3);
        User user = new User(parts[0]);
        String[] followers = parts[2].replaceAll(" ","").split(",");

        for (String follower:followers) {
            user.getFollowers().add(follower);
        }
        return user;
    };

    private Function<String,Tweet> mapTweetToUserFeeds = (line) -> {

        String [] parts = line.split(">",2);
        String twitterFeedString = line.replace(">",":");

        String username = parts[0];
        Tweet tweet = new Tweet();
        try{
            tweet.setText(twitterFeedString);

            for (User user:allUsers) {
                if(user.getUsername().equalsIgnoreCase(username) || user.getFollowers().contains(username)){
                    user.addTweet(tweet);
                }
            }
        }
        catch(Exception e){
            System.out.println("Could not map tweet to user feeds");
        }
        return tweet;
    };
}


