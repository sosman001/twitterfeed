package za.co.domain;


/**
 * Created by shabnam on 7/22/17.
 */
public class Tweet {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) throws Exception {
        if(text.length()<140) {
            this.text = text;
        }
        else
        {
            throw new Exception("Tweet length exceeds 140 characters.");
        }
    }
}
