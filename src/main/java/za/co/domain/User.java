package za.co.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shabnam on 7/21/17.
 */
public class User implements Comparable<User>{

    private String username;
    private List<Tweet> feed = new ArrayList<Tweet>();
    private List<String> followers = new ArrayList<String>();

    public User(String username) {
        this.username = username;
    }

    public List<Tweet> getFeed() {
        return feed;
    }

    public void setTweet(List<Tweet> tweets) {
        this.feed = tweets;
    }

    public List<String> getFollowers() {
        return followers;
    }

    public void setFollowers(List<String> followers) {
        this.followers = followers;
    }

    public void addTweet(Tweet tweet){
        this.feed.add(tweet);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof User)) return false;

        User user = (User) o;

        return getUsername().equals(user.getUsername());
    }

    @Override
    public int hashCode() {
        return getUsername().hashCode();
    }

    @Override
    public int compareTo(User o) {
        return username.compareTo(o.getUsername());
    }
}
