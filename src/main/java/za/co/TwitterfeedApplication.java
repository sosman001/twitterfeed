package za.co;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwitterfeedApplication {

	public static void main(String[] args) {
		SpringApplication.run(TwitterfeedApplication.class, args);
	}
}
