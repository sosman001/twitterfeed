package za.co;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.domain.Tweet;
import za.co.domain.User;
import za.co.services.TwitterFeedServiceImpl;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TwitterfeedApplicationTests {

	@Autowired
	ResourceLoader resourceLoader;

	@Autowired
	TwitterFeedServiceImpl twitterFeedService;

	Set<User> allUsers = new HashSet<>();

	@Before
	public void setup(){

		User alan = new User("Alan");
		List<String> alansFollowers = new ArrayList<String>();
		alansFollowers.add("Ward");
		alan.setFollowers(alansFollowers);
		allUsers.add(alan);

		User ward = new User("Ward");
		allUsers.add(ward);

		User martin = new User("Martin");
		List<String> martinsFollowers = new ArrayList<String>();
		martinsFollowers.add("Ward");
		martinsFollowers.add("Alan");
		martin.setFollowers(martinsFollowers);
		allUsers.add(martin);

	}

	@Test(expected = java.lang.Exception.class)
	public void testLongTweet() throws Exception{
		Tweet longtweet = new Tweet();
		longtweet.setText("Martin: I thought Martin should atleast tweet something instead of just listening to the other two guys rant on about random computer science things!");
	}

	@Test(expected = java.lang.Exception.class)
	public void testUserFileLoadError() throws Exception{

		twitterFeedService.loadUsers(resourceLoader.getResource("userFileDoesNotExist.txt"));
	}

	@Test(expected = java.lang.Exception.class)
	public void testTweetFileLoadError() throws Exception{

		twitterFeedService.loadTweets(resourceLoader.getResource("tweetFileDoesNotExist.txt"));
	}

}
