To run this twitterfeed application, simply clone this project and navigate to the project folder in your terminal.

Ensure the 'input' folder is on the directory level as the jar. This is where the input text files are found. 

Type the following:
java -jar twitterfeed-1.0.0.jar 

and voila :) 

This project uses java 1.8 and maven to build. 

 